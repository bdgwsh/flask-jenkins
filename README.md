### Установка зависимостей и запуск


Установка зависимостей:
`pip3 install -r requirements.txt`

или 

`pipenv install`

### Jenkins
Нужно включить возможность запускать джобу удаленно.


### Конфигурация
Данные для подключения к API Jenkins читаются из переменных окружения в `settings.py`:

`JENKINS_HOST`

`JENKINS_USER`

`JENKINS_PASSWORD`

### Запуск
`python3 source/main.py`


### Пример работы
![response](images/postman_1.png)

