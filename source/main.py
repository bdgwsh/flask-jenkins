from flask import request, jsonify

from source.app import get_app
from source.services import jenkins_api

app = get_app()


@app.route('/run_job', methods=['POST'])
def run_job():
    job_name = request.get_json().get('job', '')
    params = request.get_json().get('params')

    if not jenkins_api.job_exists(job_name):
        return jsonify({'detail': 'Job does not exists'}), 400

    build_number = jenkins_api.build_job(job_name, parameters=params)

    # Check queue status.
    job_info = jenkins_api.get_job_info(job_name)
    in_queue = job_info['inQueue']
    building = False

    # If job is not in queue then it's building.
    if not in_queue:
        building = jenkins_api.get_build_info(job_name, number=build_number)['building']

    status_info = {
        'in_queue': in_queue,
        'building': building
    }

    return jsonify(status_info), 200


if __name__ == '__main__':
    app.run()
