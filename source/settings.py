import os


class Config:
    JENKINS_HOST = os.environ.get('JENKINS_HOST', 'http://localhost:49001')
    JENKINS_USER = os.environ.get('JENKINS_USER', 'admin')
    JENKINS_PASSWORD = os.environ.get('JENKINS_PASSWORD', 'admin')
    JENKINS_TOKEN = os.environ.get('JENKINS_TOKEN', None)
