import jenkins

from source.settings import Config

jenkins_api = jenkins.Jenkins(Config.JENKINS_HOST, username=Config.JENKINS_HOST, password=Config.JENKINS_PASSWORD)
jenkins_api.get_whoami()
